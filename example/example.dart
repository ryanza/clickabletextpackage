import 'package:flutter/material.dart';
import 'package:clickabletext/clickabletext.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Make Clickable Text',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Make Clickable Text'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  TextEditingController controller = TextEditingController();
  List<TextSpan> richText = List<TextSpan>();

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  //when click on Hashtag it will call
  void hashtagAction() {
    print("action clicked");
  }

  //when click on mention it will call
  void mentionAction() {
    print("mention clicked");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            TextField(
              controller: controller,
              style: TextStyle(),
              decoration: InputDecoration(
                hintText: 'Write some text with #, @ ',
              ),
            ),
            RaisedButton(
              child: Text("Put it Down"),
              onPressed: () {
                setState(() {
                  richText = ClickableText(controller.text)
                      .makeHashtagAndMention(hashtagAction, mentionAction);
                });
              },
            ),
            RichText(
              text: TextSpan(
                text: "",
                style: TextStyle(color: Colors.black),
                children: richText,
              ),
            )
          ],
        ),
      ),
    );
  }
}
