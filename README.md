# clickabletext

Find Hashtag and Mention seperately and both together.
This package work for both android and IOS just use it as child of richtext.

## Getting Started

## Screenshot

<img src="https://gitlab.com/Amirmhmdi/clickabletextpackage/-/raw/master/ss1.png" height="300em" />

## Usage
To use this package just add it to pubspec.yaml and like example use it.

[Example](https://gitlab.com/Amirmhmdi/clickabletextpackage)
```yaml
    dependencies:
        flutter:
            sdk: flutter
        clickabletext:
```

### How to Use

```dart
import 'package:flutter/material.dart';
import 'package:clickabletext/clickabletext.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Make Clickable Text',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Make Clickable Text'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  TextEditingController controller = TextEditingController();
  List<TextSpan> richText = List<TextSpan>();

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  void hashtagAction() {
    print("action clicked");
  }

  void mentionAction() {
    print("mention clicked");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            TextField(
              controller: controller,
              style: TextStyle(),
              decoration: InputDecoration(
                hintText: 'Write some text with #, @ ',
              ),
            ),
            RaisedButton(
              child: Text("Put it Down"),
              onPressed: () {
                setState(() {
                  richText = FindSympol(controller.text)
                      .makeHashtagAndMention(hashtagAction, mentionAction);
                });
              },
            ),
            RichText(
              text: TextSpan(
                text: "",
                style: TextStyle(color: Colors.black),
                children: richText,
              ),
            )
          ],
        ),
      ),
    );
  }
}

```

For help getting started with Flutter, view our 
[online documentation](https://flutter.dev/docs), which offers tutorials, 
samples, guidance on mobile development, and a full API reference.
